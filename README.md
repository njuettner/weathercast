# weathercast
[![Build Status](https://travis-ci.org/njuettner/weathercast.svg?branch=master)](https://travis-ci.org/njuettner/weathercast)
Simple go weather forecast

takes one argument (city)

example: 

./forecast berlin
--------
...
```
Berlin
Sunday - Morning 34.6°C | Day 34.6°C | Night 19.84°C
Monday - Morning 23.79°C | Day 23.79°C | Night 15.35°C
Tuesday - Morning 28.33°C | Day 28.33°C | Night 19.71°C
Wednesday - Morning 23.29°C | Day 23.29°C | Night 16.44°C
Thursday - Morning 25.66°C | Day 25.66°C | Night 17.53°C
Friday - Morning 28.02°C | Day 28.02°C | Night 23.2°C
Saturday - Morning 18.79°C | Day 18.79°C | Night 14.23°C
Sunday - Morning 19.31°C | Day 19.31°C | Night 13.44°C
Monday - Morning 19.65°C | Day 19.65°C | Night 13.69°C
Tuesday - Morning 19.87°C | Day 19.87°C | Night 12.18°C
Wednesday - Morning 21.78°C | Day 21.78°C | Night 15.65°C
Thursday - Morning 29.46°C | Day 29.46°C | Night 19.63°C
Friday - Morning 24.86°C | Day 24.86°C | Night 20.33°C
Saturday - Morning 25.79°C | Day 25.79°C | Night 14.2°C
Sunday - Morning 19.4°C | Day 19.4°C | Night 12.38°C
Monday - Morning 12.38°C | Day 12.38°C | Night 12.38°C
```

