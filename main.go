package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

const (
	Sunday = iota
	Monday
	Tuesday
	Wednesday
	Thursday
	Friday
	Saturday
)

var days = [...]string{
	"Sunday",
	"Monday",
	"Tuesday",
	"Wednesday",
	"Thursday",
	"Friday",
	"Saturday",
}

var url string = "http://api.openweathermap.org/data/2.5/forecast/daily?q="
var query string = "&mode=json&units=metric&cnt="

type data struct {
	count string
	city  string
}

type openweatherapi struct {
	City struct {
		ID    int    `json:"id"`
		Name  string `json:"name"`
		Coord struct {
			Lon float64 `json:"lon"`
			Lat float64 `json:"lat"`
		} `json:"coord"`
		Country    string `json:"country"`
		Population int    `json:"population"`
	} `json:"city"`
	Cod     string  `json:"cod"`
	Message float64 `json:"message"`
	Cnt     int     `json:"cnt"`
	List    []struct {
		Dt   int `json:"dt"`
		Temp struct {
			Day   float64 `json:"day"`
			Min   float64 `json:"min"`
			Max   float64 `json:"max"`
			Night float64 `json:"night"`
			Eve   float64 `json:"eve"`
			Morn  float64 `json:"morn"`
		} `json:"temp"`
		Pressure float64 `json:"pressure"`
		Humidity int     `json:"humidity"`
		Weather  []struct {
			ID          int    `json:"id"`
			Main        string `json:"main"`
			Description string `json:"description"`
			Icon        string `json:"icon"`
		} `json:"weather"`
		Speed  float64 `json:"speed"`
		Deg    int     `json:"deg"`
		Clouds int     `json:"clouds"`
		Rain   float64 `json:"rain"`
	} `json:"list"`
}

func main() {
	t := time.Now()
	openweather := openweatherapi{}
	city := os.Args[1]
	data := data{"17", city}
	fullurl := url + data.city + query + data.count
	u, err := http.Get(fullurl)
	if err != nil {
		fmt.Println("Cannot fetch data from openweather")
		fmt.Printf("Request failed %s", fullurl)
		os.Exit(1)
	}
	defer u.Body.Close()
	output, _ := ioutil.ReadAll(u.Body)
	json.Unmarshal(output, &openweather)
	fmt.Printf("%s\n", openweather.City.Name)
	count := int(t.Weekday())
	for i, _ := range openweather.List {
		if count > 6 {
			count = 0
		}
		fmt.Printf("%s - Morning %v°C | Day %v°C | Night %v°C\n",
			days[count], openweather.List[i].Temp.Morn, openweather.List[i].Temp.Day,
			openweather.List[i].Temp.Night)
		count += 1
	}
}
